<?php

class CityController extends JOController
{
    private $view;
    private $request;
//die(print_r($this->cityController));
    public function __construct()
    {
        parent::get(array(
            'JOView',
            'JORequest'
        ));
        
        $this->view = new JOView();
        $this->request = new JORequest();
    }
  
    public function trip()  
    {
        list($cityName) = $this->request->getParams();
        $citys = CityHelper::getAll();
        
        if(!isset($citys[$cityName])){
            $this->view->render('index.phtml', array(
                'template' => 'page-not-found.phtml'
            ));
        }
        
        $this->view->render('index.phtml', array(
            'template' => 'city-trip.phtml', 
            'data' =>  $citys[$cityName]
        ));
    }

}
