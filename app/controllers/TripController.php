<?php

class TripController extends JOController
{
    private $view;

    public function __construct()
    {
        parent::get(array(
            'JOView'
        ));

        $this->view = new JOView();
    }

    public function index()
    {
        //die(print_r($this->cityController));

        $this->view->render('index.phtml', array(
             'template' => 'all-trip.phtml',
             'citys' =>  CityHelper::getAll()
        ));
    }

}
