<?php 

class TitleMetaHelper
{
	public static function getAll()
	{
		return json_decode(file_get_contents(ROOT . '/lib/data/title-meta.json'), true);	    
	}

	public static function get($key)
	{
		$titles = self::getAll();
		if(isset($titles[$key])){
			return $titles[$key];	
		}
		return null;
	}

}  